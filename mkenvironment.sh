#!/bin/bash

echo "Generating secret key base"
YOUR_SECRET_KEY_BASE=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 128 ; echo '')

echo "Generating device secret"
YOUR_DEVISE_SECRET_KEY=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 128 ; echo '')

echo "Generating database password"
YOUR_DB_PASSWORD=$(pwgen 22 1)

echo -n "Enter your EPN account: "
read YOUR_EPN_ACCOUNT

echo -n "Enter your Twitter key: "
red YOUR_TWITTER_KEY

echo -n "Enter your Twitter secret: "
read YOUR_TWITTER_SECRET

echo -n "Enter your Facebook AppID: "
read YOUR_FACEBOOK_APPID

echo -n "Enter your Facebook secret: "
read YOUR_FACEBOOK_SECRET

echo -n "Enter your Email username: "
read YOUR_MAILER_USER_NAME

echo -n "Enter your email Password: "
read YOUR_MAILER_PASSWORD

echo -n "Enter your email port: "
read YOUR_MAILER_PORT

echo -n "Enter your email hostname: "
read YOUR_MAILER_ADDRESS

echo -n "Enter your Fedex login: "
read YOUR_FEDEX_LOGIN

echo -n "Enter your Fedex password: "
read YOUR_FEDEX_PASSWORD

echo -n "Enter your Fedex key: "
read YOUR_FEDEX_KEY

echo -n "Enter your Fedex account number: "
read YOUR_FEDEX_ACCOUNT

echo -n "Enter your USPS login: "
read YOUR_USPS_LOGIN

echo -n "Enter your USPS password: "
read YOUR_USPS_PASSWORD

echo -n "Enter your UPS login: "
read YOUR_UPS_LOGIN

echo -n "Enter your UPS password: "
read YOUR_UPS_PASSWORD

echo -n "Enter your UPS key: "
read YOUR_UPS_KEY



sed s/YOUR_DB_PASSWORD/$YOUR_DB_PASSWORD/g example.env |\
sed s/YOUR_EPN_ACCOUNT/$YOUR_EPN_ACCOUNT/g |\
sed s/YOUR_TWITTER_KEY/$YOUR_TWITTER_KEY/g |\
sed s/YOUR_TWITTER_SECRET/$YOUR_TWITTER_SECRET/g |\
sed s/YOUR_FACEBOOK_APPID/$YOUR_FACEBOOK_APPID/g |\
sed s/YOUR_FACEBOOK_SECRET/$YOUR_FACEBOOK_SECRET/g |\
sed s/TOUR_MAILER_USER_NAME/$YOUR_MAILER_USER_NAME/g |\
sed s/YOUR_MAILER_PASSWORD/$YOUR_MAILER_PASSWORD/g |\
sed s/YOUR_MAILER_PORT/$YOUR_MAILER_PORT/g |\
sed s/YOUR_MAILER_ADDRESS/$YOUR_MAILER_ADDRESS/g |\
sed s/YOUR_FEDEX_LOGIN/$YOUR_FEDEX_LOGIN/g |\
sed s/YOUR_FEDEX_PASSWORD/$YOUR_FEDEX_PASSWORD/g |\
sed s/YOUR_FEDEX_KEY/$YOUR_FEDEX_KEY/g |\
sed s/YOUR_FEDEX_ACCOUNT/$YOUR_FEDEX_ACCOUNT/g |\
sed s/YOUR_USPS_LOGIN/$YOUR_USPS_LOGIN/g |\
sed s/YOUR_USPS_PASSWORD/$YOUR_USPS_PASSWORD/g |\
sed s/YOUR_UPS_LOGIN/$YOUR_UPS_LOGIN/g |\
sed s/YOUR_UPS_PASSWORD/$YOUR_UPS_PASSWORD/g |\
sed s/YOUR_UPS_KEY/$YOUR_UPS_KEY/g |\
sed s/YOUR_DEVISE_SECRET_KEY/$YOUR_DEVISE_SECRET_KEY/g |\
sed s/YOUR_SECRET_KEY_BASE/$YOUR_SECRET_KEY_BASE/g > variables.env
